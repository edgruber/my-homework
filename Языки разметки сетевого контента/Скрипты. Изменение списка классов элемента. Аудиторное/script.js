yellow.onclick = function(n) {
  let a = document.getElementsByTagName('a');

  for (i = 0; i < a.length; i++) {
    let arrElem = a[i];

    attribute = arrElem.getAttribute("href");

    if ((attribute) && (attribute.indexOf("://") != -1)) {
      if (arrElem.getAttribute("class") == "external") {
        arrElem.removeAttribute("class");
        yellow.innerText = 'Подсветить внешние ссылки';
      } else {
        arrElem.setAttribute('class', "external");
        yellow.innerText = 'Убрать подсветку с внешних ссылок';
      }
    }
  }
}