#include <iostream>
#include <list>
#include <vector>
#include <cstring>

using namespace std;

//Функция для перевода из десятичной системы в двоичную
int ASCII(int num)
{
    int binary = 0, k = 1;
    while (num)
    {
        binary += (num % 2) * k;
        k *= 10;
        num /= 2;
    }
    return binary;
}

void HammingCode ()

int main() {
  setlocale(LC_ALL, "Russian");
  string word = "word";
  int size = word.size();
  //Создание массива char для перевода в ASCII
  char ASCIIWord[size];
  //Создание массива для перевода из ASCII в двоичную систему
  int BinaryWord[size];

  //Цикл для перевода в двоичную систему
  for (int i = 0; i < size; i++) {
		ASCIIWord[i] = word[i];
    BinaryWord[i] = ASCIIWord[i];
    BinaryWord[i] = ASCII(BinaryWord[i]);
    cout << BinaryWord[i] << endl;
	}
  cout << "-----------" <<endl;
  //Основной цикл
  for (int i = 0; i < size; i++){
    vector <int> LetterCheck(12, 0);
    int degree[4] = {0, 1, 3, 7};
    for (int j = 0; j < 4; j++) {
      LetterCheck[degree[j]] = 9;
    }

    //Заполнение вектора кодом символа
    string BinaryOut = to_string(BinaryWord[i]);
    int c = 0;
    for (int j = 4; j < 12; j++) {
      if (LetterCheck[j] != 9) {
        switch ((int)(BinaryOut[c])) {
          case 48:
            LetterCheck[j] = 0;
            c++;
            break;
          case 49:
            LetterCheck[j] = 1;
            c++;
            break;
        }
      }
    }

    for (int j = 0; j < 12; j++) {
      cout << LetterCheck[j];
    }
      cout << "-----------" <<endl;
  }
}