import random

def HammingCode(Letter, errors, afterCheck):
  arr = []
  sum = 0
  end = 0
  result = ''
  print('\n\tКод символа до вычисления контрольных бит: \t  ', end = '')
  for i in Letter:
    print(i, end = '')

  for i in range(1, 12):
    if (i & (i - 1) == 0):
      if (i % 2 == 1):
        step = i + 1
      else:
        step = i * 2

      for j in range(i - 1, 12, step):
        end = i + j
        if (end > 12):
          end = 12
        arr.append(Letter[j:end])

      for j in range(len(arr)):
        for c in range(len(arr[j])):
          result += str(arr[j][c])
      result = result[1:]
      result = int(result)
      while result > 0:
        digit = result % 10
        sum += digit
        result //= 10
      
      if (sum % 2 == 0):
        num = 0
      else:
        num = 1
      if (afterCheck == 0):
        Letter[i-1] = str(num)
      else:
        if Letter[i - 1] != str(num):
          errors += i
      sum = 0
      arr = []
      result = ''
  if (afterCheck == 0):
    print('\n\tКод символа после вычисления контрольных бит: ', end = '')
    for i in Letter:
      print(i, end = '')
  if (afterCheck != 0 and errors !=0):
    print("\n\tИндекс ошибки: ", errors - 1)
  elif (afterCheck != 0 and errors == 0):
    print("\n\tОшибок в данном символе нет")


#Функция для перевода слова в ASCII
def ASCII(letter):
  output = ''
  for i in range(8):
    output = str(letter % 2) + output
    letter //= 2
  return output

word = input('Введите слово: ')

ASCIIWord = []
LetterCheck = []

for i in word:
  ASCIIWord.append(ASCII(ord(i)))

print(word, "\n\nПредставление слова", word, "в ASCII:")
print("\t", end ='')
print(*ASCIIWord, sep = ",")
print('')

for i in range(len(ASCIIWord)):
  LetterCheck.append(list(ASCIIWord[i]))

  #Заполнение степеней двойки 'x'
  for j in range(12):
    if (j & (j - 1) == 0):
      LetterCheck[i].insert(j, 'x')
  LetterCheck[i].pop(0)
  afterCheck = 0
  errors = 0
  print("\nСимвол", word[i], "без ошибок:")
  HammingCode(LetterCheck[i], errors, afterCheck)
  afterCheck = 1
  rand = random.choice([2, 5, 6, 8, 9, 10, 11])
  if LetterCheck[i][rand] == "1":
    LetterCheck[i][rand] = "0"
  else:
    LetterCheck[i][rand] = "1"

  print("\n\tСлучайный индекс: ", rand)
  
  print("\nСимвол", word[i], "c ошибкой:")
  HammingCode(LetterCheck[i], errors, afterCheck)
  afterCheck = 0