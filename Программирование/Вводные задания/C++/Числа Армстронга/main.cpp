#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>

using namespace std;

int num_count(int i){
  int length = 0;
  while (i > 0) {
    i /= 10;
    length++;
  }
  return length;
}

int main() {
  setlocale(LC_ALL, "Russian");
  cout << "Числа Армстронга (самовлюбленные числа) – числа, равные сумме своих цифр, каждая из которых возведена в степень, равную количеству цифр в числе. \nЗадача: \nНайти все числа Армстронга в промежутке от 1 до 32 000." << endl;
  int j = 0;
  int length = 0, s = 0;
  for (int i = 1; i < 32000; i++){
    length = num_count(i);
    j = i;

    while (j){
      s += pow(j % 10, length);
      j /= 10;
    }

    if (s == i) {
      cout << s << ", ";
    }

    s = 0;
    j = 0;
  }
}