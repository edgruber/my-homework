import math

while True:
  number = int(input("Введите число от 0 до 200 в десятичной системе счисления: "))
  if number <= 201 and number >= 0:
    break
numberArr = []
result = 0

while (number > 0):
  numberArr.append(number % 3)
  numberArr.append(number % 3)
  number = math.floor(number / 3)

for i in range(len(numberArr)):
  result += numberArr[i] * pow(3, i)

print("Закодированное число:", result)