#include <iostream>
#include <list>
#include <cmath>

using namespace std;
int main() {
  int number, result = 0, i = 0;
  list <int> number_arr;
  list <int> :: iterator it;

  do{
    cout << "Введите число от 0 до 200 в десятичной системе счисления: ";
    cin >> number;
  } while (number <= 0 || number >= 200);
  number = 92;

  while (number > 0) {
    number_arr.push_back(number % 3);
    number_arr.push_back(number % 3);
    number = floor(number / 3);
  }

  for (it = number_arr.begin(); it != number_arr.end(); it++) { 
    result += *it * pow(3, i);
    i++;
  }

  cout << "Закодированное число: " << result;
}