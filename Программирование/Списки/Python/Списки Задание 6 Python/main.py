import random
print('В метании молота состязается n спортcменов. Каждый из них сделал m бросков. Победителем считается тот спортсмен, у которого сумма результатов по всем броскам максимальна.')

while True:
  n = int(input('Введите количество спортсменов: '))
  if n > 0:
    break
while True:
  m = int(input('Введите количество бросков: '))
  if m > 0:
    break
A = [[random.randint(0, 5) for j in range(m)] for i in range(n)]

#Дается начальное значение максимума

maxA = A[0][0]
player = A[0]

#Вывод списка
print()
for i in range(n):
  print('Броски игрока №', i + 1, A[i])

if m == 0:
  for i in range(n):
    if A[i][0] > maxA:
      maxA = A[i][0]
      player = i + 1
else:
  for i in range(n):
    for j in range(m):
      if sum(A[i]) > maxA:
        maxA = sum(A[i])
        player = i + 1

print('Победивший игрок:', player, '\nСумма очков:', maxA)