#include <iostream>
#include <cstring>

using namespace std;

int main() {
  setlocale(LC_ALL, "Russian");
  string cg;
  cout << "Введите геномную последовательность: ";
  getline(cin, cg);
  for(int i = 0; i < cg.length(); i++) {
      if(cg[i] == ' ') {
        cg.erase(i,1);
        i--;
    }
  }
  cout << cg;
  double s = 0;
  for (int i = 0; i < cg.length(); i++){
    if ((cg[i] == 'c') || (cg[i] == 'g') || (cg[i] == 'C') || (cg[i] == 'G')){
      s++;
    }
  }
  cout << "\nПроцентное содержание гуанина и цитозина в данной геномной последовательности: " << s/cg.length()*100 << "%";

}