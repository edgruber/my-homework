#include <iostream>
#include <cstring>

using namespace std;

int main() {
  setlocale(LC_ALL, "Russian");
  cout << "В метании молота состязается n спортcменов. Каждый из них сделал m бросков. Победителем считается тот спортсмен, у которого сумма результатов по всем броскам максимальна." << endl;

  int n, m;
  do {
    cout << "Введите количество спортсменов: ";
    cin >> n;
  } while (n < 1);
  do {
    cout << "Введите количество бросков: ";
    cin >> m;
  } while (m < 1);
  int A [n][m];

  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++){
      A[i][j] = random() % 5;
    }
  }
  
  int maxA = A[0][0];
  int player = 0;

  for(int i = 0; i < n; i++){
    cout << "\nБроски игрока № " << i + 1 << " [";
    for(int j = 0; j < m; j++){
      cout << A[i][j] << ", ";
    }
    cout << "]";
  }

  if (m == 0) {
    for(int i = 0; i < n; i++){
      if (A[i][0] > maxA){
        maxA = A[i][0];
        player = i + 1;
      }
    }
  }
  else{
    for(int i = 0; i < n; i++){
      int s = 0;
      for(int j = 0; j < m; j++){
        s += A[i][j];
      }
      if (s > maxA){
        maxA = s;
        player = i + 1;
      }
    }
  }

  cout << "\nПобедивший игрок: " << player << "\nСумма очков: " << maxA;

}