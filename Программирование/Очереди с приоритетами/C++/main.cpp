#include <iostream>
#include <list>

using namespace std;

class Heaps {
    public:
      int size = 15;
      int *que = new int [size];
      bool isfull = true;

      //Добавление элементов из списка
      void heappush(list <int> arr){
        int j = 0;
        for(int i: arr){
          que[j] = i;
          j++;
        }
      }

      void heapify(int n, int i){
        int HeapMax = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        if (left < n && que[left] > que[HeapMax]){
          HeapMax = left;
        }

        if (right < n && que[right] > que[HeapMax]){
          HeapMax = right;
        }

        if (HeapMax != i) {
          int temp = que[i];
          que[i] = que[HeapMax];
          que[HeapMax] = temp;
          heapify(n, HeapMax);
        }
      }

      void HeapSort() {
        int n = size;

        for(int i = n; i > -1; i--){
          heapify(n, i);
        }

        for(int i = n - 1; i > 0; i--){
          int temp = que[i];
          que[i] = que[0];
          que[0] = temp;
          heapify(i, 0);
        }
      }

      void Children(){
        for (int i = 0; i <= size; i++){
          int left_child = 2 * i + 1;
          int right_child = 2 * i + 2;

          if(((right_child < size) && (left_child >= size)) || ((left_child < size) && (right_child >= size))) {
            isfull = false;
          }
        }
      }

      void IsFull(){
        Children();
        if (isfull == true) {
          cout << "Дерево полное";
        }
        else {
          cout << "Дерево неполное";
        }
      }
};

int main() {
  setlocale(LC_ALL, "Russian");
  list <int> arr = {2, 3, 5, 1, 4, 6, 8, 9, 0, 7, 10, 13, 14, 12, 11};
  Heaps heap;
  heap.size = arr.size();
  heap.heappush(arr);
  for(int i = 0; i < heap.size; i++) {
    cout <<heap.que[i] << " ";
  }
  cout << endl;
  heap.HeapSort();
  for(int i = 0; i < heap.size; i++) {
    cout <<heap.que[i] << " ";
  }

  heap.IsFull();

  
}
