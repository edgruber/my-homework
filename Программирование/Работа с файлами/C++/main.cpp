#include <iostream>
#include <fstream>
#include <cstring>
#include <map>
#include <list>

using namespace std;

//Функция для подсчёта строк в документе
int lines_count(string a) {
  int i = 0;
  string line;
  ifstream open(a);
  while (getline(open, line)) {
    i += 1;
  }
  open.close();
  return i;
}


int main() {
  setlocale(LC_ALL, "Russian");
  string line, file_name = "texxxt.txt";
  int lines;

  //Открытие файла, подсчёт строк и создание списков
  ifstream input_file;
  input_file.open(file_name);
  lines = lines_count(file_name);
  list<string> text_arr;
  list<string> text_arr_second;
  list<string> roles;
  list<string> phrases;
  multimap <string, string> text_dict;

  //Заполнение списка строками
  while (getline(input_file, line)) {
    text_arr.push_back(line);
    phrases.push_back(line);
  }
  //Заполнение списка ролями
  for (auto iter = text_arr.begin(); iter != text_arr.end(); iter++)
    {
      if (*iter == "textLines:"){
        phrases.pop_front();
        break;
      }
      if (*iter != "roles:") {
        roles.push_back(*iter);
      }
      phrases.pop_front();
    }
  roles.push_back("Автор:");

  for (auto iter = roles.begin(); iter != roles.end(); iter++)
    {
        *iter += ": ";
    }


  //Нумерация строк
  int index = 1;
  for (auto iter = phrases.begin(); iter != phrases.end(); iter++)
  {
      
      *iter = to_string(index) + ". " + *iter;
      index++;
  }

//Цикл для слов автора
for (auto j = phrases.begin(); j != phrases.end(); j++)
{
  string strj = *j;
  if (strj.find("(") != string::npos)
  {
    int bracket = strj.find('(');
    int end = 0;
    int numend = strj.find('.') + 2;
    for (int i = bracket; i < strj.length(); i++)
      {
        if (strj[i] == ')')
        {
            end = i - 1;
            break;
        }
      }
    string author_phrase = strj.substr(bracket + 1, end - bracket);
    string line_num = strj.substr(0, numend);
    text_dict.emplace("Автор: ", line_num + author_phrase);
    strj.replace(bracket, end, "");
    *j = strj;
  }
}
//Основной цикл
  for (auto i = roles.begin(); i != roles.end(); i++)
  {
      string stri = *i;
      int last_role;
      for (auto j = phrases.begin(); j != phrases.end(); j++)
      {
        string strj = *j;
        if (strj.find(stri) != string::npos)
        {
          strj.replace(3, stri.length(), "");
          text_dict.emplace(stri, strj);
          last_role = 1;
          continue;
        }
        if (last_role == 1) {
          if ((strj.find(":") == string::npos)) {
            text_dict.emplace(stri, strj);
          }
          else {
            last_role = 0;
          }
        }
      }
    }

  //Вывод на экран
  string empty = "empty";
  for (const auto& i : text_dict)
  {
      if (empty != i.first)
      {
        cout << endl << "-----------------------------------" << endl;
        cout << i.first << endl;
        empty = i.first;
        cout << "-----------------------------------" << endl << endl;
      }
      cout << i.second << endl;
  }

//Вывод в txt
  fstream output_file;
  output_file.open("output.txt");
    string empty_file = "empty";
    for (const auto& i : text_dict)
    {
        if (empty_file != i.first)
        {
          output_file << endl << "-----------------------------------" << endl;
          output_file << i.first << endl;
          empty_file = i.first;
          output_file << "-----------------------------------" << endl << endl;
        }
        output_file << i.second << endl;
    }

  output_file.close();
  input_file.close();
}