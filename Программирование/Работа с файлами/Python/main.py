text_input = open("text.txt")

textDict = dict()

textArray = text_input.readlines()
roles = []
phrases = []

#Удаление "\n" в конце строк
textArray = [line.rstrip() for line in textArray]

#Заполнение массива ролями
for i in range(1, textArray.index("textLines:")):
  roles.append(textArray[i])
roles = [role + ":" for role in roles]

#Заполнение массива фразами
for i in range(textArray.index("textLines:") + 1, len(textArray)):
  phrases.append(textArray[i])

#Цикл для нумерации строк
for role in roles:
  textDict[role] = []
  for phrase in range(0, len(phrases)):
    if (phrases[phrase].startswith(role)):
      phrases[phrase] = phrases[phrase].replace(role + " ", "")
      phrases[phrase] = role + " "+ str(phrase+1) + ") " + phrases [phrase]

phrases2 = phrases.copy()

#Цикл для заполнения словаря
for role in roles:
  textDict[role] = []
  for phrase in phrases:
    if (phrase.startswith(role)):
      phrase_replaced = phrase.replace(role + " ", "")
      textDict[role].append(phrase_replaced)
      phrases2.remove(phrase)

#Цикл для реплик автора
textDict["Автор:"] = []
for phrase in phrases2:
    textDict["Автор:"].append(phrase)

#Вывод
for i in list(textDict.keys()):
  print("\n" + i)
  for j in textDict[i]:
	  print(j)